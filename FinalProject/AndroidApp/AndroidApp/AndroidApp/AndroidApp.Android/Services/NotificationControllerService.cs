﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using AndroidApp.Messages;
using Xamarin.Forms;

namespace AndroidApp.Droid.Services
{
    [Service]
    class NotificationControllerService : Service
    {
        public override IBinder OnBind(Intent intent)
        {
            return null;
        }

        [return: GeneratedEnum]
        public override StartCommandResult OnStartCommand(Intent intent, [GeneratedEnum] StartCommandFlags flags, int startId)
        {
            
            int taskId = (int)intent.GetIntExtra("taskId", -1);
            var debugMsg = "";
            var msg = new TaskMessage()
            {
                TaskId = taskId
            };

            switch(intent.Action)
            {
                case "OKMESSAGE":
                    debugMsg = "OK";
                    MessagingCenter.Send(msg, "OkMessage");
                    break;
                case "SNEEZEMESSAGE":
                    debugMsg = "SNEEZE";
                    MessagingCenter.Send(msg, "SneezeMessage");
                    break;
                case "NAVIGATEMESSAGE":
                    debugMsg = "NAVIGATE";
                    MessagingCenter.Send(msg, "NavigateMessage");
                    break;
                default:
                    break;
            }

            MessagingCenter.Send(debugMsg, "DebugMessage");
            var notificationManager = NotificationManagerCompat.From(Android.App.Application.Context);
            notificationManager.Cancel(71830);
            return StartCommandResult.Sticky;
        }
    }
}