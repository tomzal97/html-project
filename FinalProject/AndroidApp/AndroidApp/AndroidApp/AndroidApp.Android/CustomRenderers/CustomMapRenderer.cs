﻿using Android.App;
using Android.Content;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AndroidApp.Droid.CustomRenderers;
using AndroidApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms.GoogleMaps.Android;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomMap), typeof(CustomMapRenderer))]
namespace AndroidApp.Droid.CustomRenderers
{
    class CustomMapRenderer : MapRenderer, GoogleMap.IInfoWindowAdapter
    {
        List<UserTask> userTasks;

        public CustomMapRenderer(Context context) : base(context)
        {

        }

        protected override void OnElementChanged(ElementChangedEventArgs<Map> e)
        {
            base.OnElementChanged(e);
            if (e.NewElement != null)
            {
                var formsMap = (CustomMap)e.NewElement;
                userTasks = formsMap.UserTasks;
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (e.PropertyName.Equals("VisibleRegion"))
            {
                NativeMap.SetInfoWindowAdapter(this);
            }
        }

        public Android.Views.View GetInfoContents(Marker marker)
        {
            var inflater = Android.App.Application.Context.GetSystemService(Context.LayoutInflaterService) as Android.Views.LayoutInflater;
            if (inflater != null)
            {
                Android.Views.View view;

                view = inflater.Inflate(Resource.Layout.MapInfoWindow, null);

                var task = GetTask(marker);

                var infoTitle = view.FindViewById<TextView>(Resource.Id.InfoWindowTitle);
                var infoSubtitle = view.FindViewById<TextView>(Resource.Id.InfoWindowSubtitle);
                var infoType = view.FindViewById<TextView>(Resource.Id.InfoWindowType);
                var infoDate = view.FindViewById<TextView>(Resource.Id.InfoWindowDate);

                if (infoTitle != null)
                {
                    infoTitle.Text = task.Title;
                }
                if (infoSubtitle != null)
                {
                    if (String.IsNullOrWhiteSpace(task.Description))
                        infoSubtitle.Visibility = ViewStates.Gone;
                    else
                        infoSubtitle.Text = task.Description;
                }
                if (infoType != null)
                {
                    infoType.Text = "Task type: " + task.Type.ToString();
                }
                if (infoDate != null)
                {
                    if (task.Type == Models.Enums.UserTaskType.Point)
                        infoDate.Text = "Meeting time: " + task.DateFrom.ToShortDateString() + " " + task.MeetingTime.ToString();
                    else
                        infoDate.Text = "Valid From: " + task.DateFrom.ToShortDateString() + System.Environment.NewLine +
                            "Valid To: " + task.DateTo.ToShortDateString();
                }

                return view;
            }
            return null;
        }

        private UserTask GetTask(Marker marker)
        {
            var position = new Position(marker.Position.Latitude, marker.Position.Longitude);
            var task = userTasks.Where(ut => ut.Pin.Position == position).FirstOrDefault();
            return task;
        }

        public Android.Views.View GetInfoWindow(Marker marker)
        {
            return null;
        }

    }
}