﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using AndroidApp.Droid;
using AndroidApp.Droid.Services;
using AndroidApp.Interfaces;

[assembly: Xamarin.Forms.Dependency(typeof(OnPlaceNotification))]
namespace AndroidApp.Droid
{
    public class OnPlaceNotification : IOnPlaceNotifcation
    {
        public void ShowNotification(int taskId, string message)
        {
            var notification = new NotificationCompat.Builder(Application.Context, Constants.CHANNEL_ID)
                .SetContentTitle("GeoTask")
                .SetContentText(message)
                .SetSmallIcon(Resource.Drawable.notification)
                .AddAction(BuildSneezeAction(taskId))
                .AddAction(BuildNavigationAction(taskId))
                .AddAction(BuildOkAction(taskId))
                .SetStyle(new NotificationCompat.BigTextStyle().BigText(message))
                .Build();

            notification.Flags = NotificationFlags.AutoCancel;
            var notificationManager = NotificationManagerCompat.From(Application.Context);
            notificationManager.Notify(71830, notification);
            
        }

        private NotificationCompat.Action BuildOkAction(int taskId)
        {
            var okIntent = new Intent(Application.Context, typeof(NotificationControllerService));
            okIntent.SetAction("OKMESSAGE");
            okIntent.PutExtra("taskId", taskId);
            okIntent.SetFlags(ActivityFlags.SingleTop | ActivityFlags.ClearTask);

            var stopServicePendingIntent = PendingIntent.GetService(Application.Context, 0, okIntent, PendingIntentFlags.OneShot);

            var builder = new NotificationCompat.Action.Builder(Resource.Drawable.notification,
                                                          "ok",
                                                          stopServicePendingIntent);
            return builder.Build();
        }

        private NotificationCompat.Action BuildNavigationAction(int taskId)
        {
            var navigationIntent = new Intent(Application.Context, typeof(NotificationControllerService));
            navigationIntent.SetAction("SNEEZEMESSAGE");
            navigationIntent.PutExtra("taskId", taskId);
            navigationIntent.SetFlags(ActivityFlags.SingleTop | ActivityFlags.ClearTask);

            var stopServicePendingIntent = PendingIntent.GetService(Application.Context, 0, navigationIntent, 0);

            var builder = new NotificationCompat.Action.Builder(Resource.Drawable.notification,
                                                          "SNOOZE",
                                                          stopServicePendingIntent);
            return builder.Build();
        }

        private NotificationCompat.Action BuildSneezeAction(int taskId)
        {
            var sneezeIntent = new Intent(Application.Context, typeof(NotificationControllerService));
            sneezeIntent.SetAction("NAVIGATEMESSAGE");
            sneezeIntent.PutExtra("taskId", taskId);
            sneezeIntent.SetFlags(ActivityFlags.SingleTop | ActivityFlags.ClearTask);

            var stopServicePendingIntent = PendingIntent.GetService(Application.Context, 0, sneezeIntent, 0);

            var builder = new NotificationCompat.Action.Builder(Resource.Drawable.notification,
                                                          "NAVIGATE",
                                                          stopServicePendingIntent);
            return builder.Build();
        }
    }
}