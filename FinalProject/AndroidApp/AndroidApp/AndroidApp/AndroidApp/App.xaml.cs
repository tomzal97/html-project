﻿using AndroidApp.Interfaces;
using AndroidApp.Messages;
using AndroidApp.Models;
using AndroidApp.Models.Enums;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Plugin.LocalNotifications;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms.Xaml;
using API;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace AndroidApp
{
    public partial class App : Application
    {
        public static Plugin.Geolocator.Abstractions.Position Location { get; set; }

        public static ObservableCollection<UserTask> userTasks = new ObservableCollection<UserTask>();

        public App()
        {
            InitializeComponent();

            var record = API.API.GetAsync().Result;
            foreach (var v in record)
            {
                var str = v.TravelType;
                str = char.ToUpper(str[0]) + str.Substring(1);
                userTasks.Add(new UserTask
                {
                    DateFrom = v.StartTime,
                    DateTo = v.EndTime ?? v.StartTime,
                    Description = v.Description,
                    Pin = new Pin
                    {
                        Type = PinType.Place,
                        Position = new Xamarin.Forms.GoogleMaps.Position(v.LocationLatLng.Latitude, v.LocationLatLng.Longitude),
                        Label = v.Title,
                        Address = v.Description
                    },
                    Radius = (double)(v.Radius ?? 0),
                    Title = v.Title,
                    Type = v.Type == 1 ? UserTaskType.Point : UserTaskType.Radius,
                    Id = v.Id,
                    MeetingTime = v.StartTime.TimeOfDay,
                    TravelMode = (TravelType)Enum.Parse(typeof(TravelType), str)
                });

            }

            MainPage = new MainPage();
            SubscribeMessages();
        }

        private void SubscribeMessages()
        {
            MessagingCenter.Subscribe<PositionMessage>(this, "PositionMessage", PositionMessageResponse());
            //MessagingCenter.Subscribe<String>(this, "DebugMessage", DebugMessageResponse());
            MessagingCenter.Subscribe<TaskMessage>(this, "OkMessage", OkMessageResponse());
            MessagingCenter.Subscribe<TaskMessage>(this, "SneezeMessage", SneezeMessageResponse());
            MessagingCenter.Subscribe<TaskMessage>(this, "NavigateMessage", NavigateMessageResponse());
        }

        private Action<TaskMessage> SneezeMessageResponse()
        {
            //TODO: ADD SNEEZE
            return async  message => { await API.API.SnoozeAsync(message.TaskId); };
        }

        private Action<TaskMessage> NavigateMessageResponse()
        {
            return message =>
            {
                var task = userTasks.Where(ut => ut.Id == message.TaskId).FirstOrDefault();
                var location = task.Pin.Position;
                var locationString = Uri.EscapeUriString($"destination={location.Latitude},{location.Longitude}");
                var travelModeString = Uri.EscapeUriString($"travelmode={task.TravelMode.ToString().ToLower()}");

                var uri = new Uri($"https://www.google.com/maps/dir/?api=1&"+locationString + "&" + travelModeString);
                Device.OpenUri(uri);
            };
        }

        private Action<TaskMessage> OkMessageResponse()
        {
            return async message =>
            {
                var del = App.userTasks.FirstOrDefault(u => u.Id == message.TaskId);
                App.userTasks.Remove(del);
                await API.API.DeleteAsync(message.TaskId);
            };
        }

        private static Action<string> DebugMessageResponse()
        {
            return message =>
            {
                DependencyService.Get<IMessage>().LongAlert(message);
            };
        }

        private static Action<PositionMessage> PositionMessageResponse()
        {
            return async message =>
            {
                API.Model.LocationLatLng pos = new API.Model.LocationLatLng()
                {
                    Longitude = message.Location.Longitude,
                    Latitude = message.Location.Latitude
                };
                Location = message.Location;

                var results = await API.API.GetWithinRadiusAsync(pos);

                foreach (var result in results)
                {
                    DependencyService.Get<IOnPlaceNotifcation>().ShowNotification(result.TaskId, result.Message);
                    //DependencyService.Get<IMessage>().LongAlert(result.Message);
                    //CrossLocalNotifications.Current.Show("GeoTask", result.Message);
                }
            };
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }


    }
}
