﻿using AndroidApp.Interfaces;
using AndroidApp.Messages;
using AndroidApp.Models;
using AndroidApp.Pages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;

namespace AndroidApp
{
    public partial class MainPage : TabbedPage
    {
        public MainPage()
        {
            InitializeComponent();
            ObservableCollection<UserTask> userTasks = new ObservableCollection<UserTask>(); 
            var mapPage = new NavigationPage(new MapPage())
            {
                Title = "Map"
            };

            var taskPage = new NavigationPage(new TaskListPage())
            {
                Title = "Tasks list"
            };


            Children.Add(mapPage);
            Children.Add(taskPage);
        }
    }
}
