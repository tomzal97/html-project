﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using API.Model;
using Newtonsoft.Json;
using System.Linq;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;

namespace API
{
    public static class API
    {
        private static readonly string uri = "https://html2postgis.azurewebsites.net/";


        public static async Task<List<Notification>> GetAsync()
        {
            var httpClient = new HttpClient();
            var response =  httpClient.GetAsync(uri+ "api/values").Result;

            //will throw an exception if not successful
            response.EnsureSuccessStatusCode();

            string content = response.Content.ReadAsStringAsync().Result;
          
            var tasks = JsonConvert.DeserializeObject<List<Notification>>(content);

            return await Task.Run(() => tasks);
        }

     

        public static async Task<int> PostAsync(LiteNotification ln)
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri(uri);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var content = new StringContent(JsonConvert.SerializeObject(ln), Encoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PostAsync("api/values", content);
            if (response.IsSuccessStatusCode)
            {
                // Console.WriteLine("Data posted");
            

            }
            else
            {
               // Console.WriteLine($"Failed to poste data. Status code:{response.StatusCode}");

            }
            string contentint = response.Content.ReadAsStringAsync().Result;
            var id = JsonConvert.DeserializeObject<int>(contentint);
            return await Task.Run(() => id);
        }

        
        //point+radius
        public static async Task<List<PushMessage>> GetWithinRadiusAsync(LocationLatLng loc)
        {


            //will throw an exception if not successful
            try
            {
                var httpClient = new HttpClient();
                var uridist = (uri + "api/values/withindist?latitude=" + loc.Latitude.ToString().Replace(',', '.') + "&longitude=" + loc.Longitude.ToString().Replace(',', '.'));
                var response = await httpClient.GetAsync(uridist);

                response.EnsureSuccessStatusCode();

                string content = await response.Content.ReadAsStringAsync();

                var tasks = JsonConvert.DeserializeObject<List<PushMessage>>(content);

                return await Task.Run(() => tasks);
            }
            catch
            {
                return await Task.FromResult<List<PushMessage>>(new List<PushMessage>());
            }
        }


        //public static async Task<List<PushMessage>> GetWithinPointAsync(LocationLatLng loc)
        //{
        //    var httpClient = new HttpClient();
        //    var uridist = (uri + "api/values/travel?latitude=" + loc.Latitude.ToString().Replace(',', '.') + "&longitude=" + loc.Longitude.ToString().Replace(',', '.'));
        //    var response = await httpClient.GetAsync(uridist);


        //    //will throw an exception if not successful
        //    response.EnsureSuccessStatusCode();

        //    string content = await response.Content.ReadAsStringAsync();

        //    var tasks = JsonConvert.DeserializeObject<List<PushMessage>>(content);

        //    return await Task.Run(() => tasks);
        //}

        public static async Task DeleteOld()
        {
            var httpClient = new HttpClient();
            var uridist = (uri + "api/values/DeleteOld");
            var response = await httpClient.DeleteAsync(uridist);


            //will throw an exception if not successful
            response.EnsureSuccessStatusCode();


        }
        public static async Task DeleteAsync(int id)
        {
            var httpClient = new HttpClient();

            var uridist = (uri + "api/values/"+id);
            var response = await httpClient.DeleteAsync(uridist);


            //will throw an exception if not successful
            response.EnsureSuccessStatusCode();


        }

        public static async Task SnoozeAsync(int id)
        {
            var httpClient = new HttpClient();
            var uridist = (uri + "api/values/snooze/" + id);
            var content = new StringContent("", Encoding.UTF8, "application/json");
            var response = await httpClient.PutAsync(uridist,content);



            //will throw an exception if not successful
            response.EnsureSuccessStatusCode();


        }

        public static async Task UpdateAsync(int id, LiteNotification ln)
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri(uri);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var content = new StringContent(JsonConvert.SerializeObject(ln), Encoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PutAsync("api/values/" + id, content);
            if (response.IsSuccessStatusCode)
            {
                //


            }
            else
            {
                //

            }
        }



    }
}
