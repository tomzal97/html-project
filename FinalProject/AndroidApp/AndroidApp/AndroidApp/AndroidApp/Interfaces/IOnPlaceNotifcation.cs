﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AndroidApp.Interfaces
{
    public interface IOnPlaceNotifcation
    {
        void ShowNotification(int taskId, string message);
    }
}
