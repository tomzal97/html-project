﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Model
{
    public class PushMessage
    {
        public int TaskId { get; set; }
        public string Message { get; set; }
    }
}
