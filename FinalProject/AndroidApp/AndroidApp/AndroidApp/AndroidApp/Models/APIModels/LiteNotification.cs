﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Model
{
    public class LiteNotification
    {

        public string Description { get; set; }
        public string Title { get; set; }
        //
        public LocationLatLng Location;
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int Type { get; set; }
        public float? Radius { get; set; }
        public string TravelType { get; set; }
    }

    public class LocationLatLng
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
