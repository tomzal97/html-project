﻿using System;
using System.Collections.Generic;


namespace API.Model
{

    public partial class Notification
    {

        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public int Type { get; set; }
        public float? Radius { get; set; }
        public string Title { get; set; }

        public DateTime? LastPush { get; set; }

        public string LastMessage { get; set; }
        public bool? PushActive { get; set; }
        public string TravelType { get; set; }



        public LocationLatLng LocationLatLng { get; set; }

        public Type TypeN { get; set; }
    }
}
