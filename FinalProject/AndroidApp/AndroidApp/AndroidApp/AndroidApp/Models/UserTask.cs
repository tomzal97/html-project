﻿using AndroidApp.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms.GoogleMaps;

namespace AndroidApp.Models
{
    public class UserTask : BaseModel
    {
        private string _title;
        private UserTaskType _type;
        private DateTime _dateFrom = DateTime.Now;
        private DateTime _dateTo = DateTime.Now.AddDays(1);
        private double _radius;
        private string _description;
        private Pin _pin;
        private int _id;
        private TravelType _navigationMode = TravelType.Walking;
        private TimeSpan _meetingTime = TimeSpan.Zero;

        public string Title { get => _title; set => SetProperty(ref _title, value); }

        public UserTaskType Type { get => _type; set => SetProperty(ref _type, value); }

        public DateTime DateFrom { get => _dateFrom; set => SetProperty(ref _dateFrom, value); }

        public DateTime DateTo { get => _dateTo; set => SetProperty(ref _dateTo, value); }

        public TimeSpan MeetingTime { get => _meetingTime; set => SetProperty(ref _meetingTime, value); }

        public double Radius { get => _radius; set => SetProperty(ref _radius, value); }

        public string Location
        {
            get
            {
                if (Pin != null && Pin.Position != null)
                    return $"Longitude: {Pin.Position.Longitude} Latitude: {Pin.Position.Latitude}";
                else
                    return "No pin set";
            }
        }

        public string Description { get => _description; set => SetProperty(ref _description, value); }

        public Pin Pin { get => _pin; set => SetProperty(ref _pin, value); }

        public int Id { get => _id; set => SetProperty(ref _id, value); }

        public TravelType TravelMode { get => _navigationMode; set => SetProperty(ref _navigationMode, value); }
    }
}
