﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms.GoogleMaps;

namespace AndroidApp.Models
{
    public class CustomMap : Map
    {
        public List<UserTask> UserTasks { get; set; } = new List<UserTask>();
    }
}
