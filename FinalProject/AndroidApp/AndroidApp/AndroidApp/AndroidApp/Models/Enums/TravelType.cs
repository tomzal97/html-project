﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AndroidApp.Models.Enums
{
    public enum TravelType
    {
        Walking,
        Driving,
        Bicycling,
        Transit
    }
}
