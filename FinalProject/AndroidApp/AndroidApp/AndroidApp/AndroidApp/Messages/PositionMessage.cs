﻿using Plugin.Geolocator.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace AndroidApp.Messages
{
    public class PositionMessage
    {
        public Position Location { get; set; }
    }
}
