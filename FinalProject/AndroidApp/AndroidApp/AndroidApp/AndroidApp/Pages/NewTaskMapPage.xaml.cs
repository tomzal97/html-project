﻿
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using AndroidApp.Models;
using Plugin.Geolocator;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms.Xaml;

namespace AndroidApp.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NewTaskMapPage : ContentPage, INotifyPropertyChanged
	{
        Position warsawLocation = new Position(52.229676, 21.012229);

        public bool IsPositionSet { get; set; } = false;

        private UserTask task;

        public NewTaskMapPage(UserTask task)
        {
            InitializeComponent();
            this.task = task;
            myMap.UiSettings.MyLocationButtonEnabled = true;
            CenterMyLocation();
        }

        private void MapTapped(object sender, MapClickedEventArgs e)
        {
            var pin = new Pin
            {
                Type = PinType.Place,
                Position = e.Point,
                Label = "Task place",
            };

            task.Pin = pin;
            myMap.Pins.Clear();
            myMap.Pins.Add(pin);
            selectButton.IsVisible = true;
        }

        private async void CenterMyLocation()
        {
            var pos = await CrossGeolocator.Current.GetPositionAsync();
            myMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(pos.Latitude, pos.Longitude), Distance.FromKilometers(2)));
            myMap.MyLocationEnabled = true;
        }

        private void selectButton_Clicked(object sender, System.EventArgs e)
        {
            OnPropertyChanged(nameof(task.Location));
            Navigation.PopModalAsync();
        }
    }
}