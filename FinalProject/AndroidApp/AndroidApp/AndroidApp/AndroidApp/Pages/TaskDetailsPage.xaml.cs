﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AndroidApp.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using API;
using AndroidApp.Models.Enums;
using AndroidApp.Interfaces;

namespace AndroidApp.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TaskDetailsPage : ContentPage
    {
        private ObservableCollection<UserTask> userTasks;

        UserTask task = new UserTask();

        bool _update = false;

        public TaskDetailsPage(bool update = false, UserTask updateTask = null)
        {
            InitializeComponent();
            this.userTasks = App.userTasks;
            _update = update;

            if (update)
            {
                addButton.Text = "Update task";
                removeButton.IsVisible = true;
                if (updateTask != null)
                {
                    task = updateTask;
                    
                }
            }

            typePicker.SelectedIndex = (int)task.Type;
            travelPicker.SelectedIndex = (int)task.TravelMode;
            BindingContext = task;
        }

        private async void AddTask(object sender, EventArgs e)
        {
            if(!IsValidForm())
            {
                return;
            }
            task.Pin.Label = task.Title;
            task.Pin.Address = $"{task.Description}";
            var model = new API.Model.LiteNotification
            {
                Description = task.Description,
                EndTime = task.DateTo,
                Location = new API.Model.LocationLatLng
                {
                    Latitude = task.Pin.Position.Latitude,
                    Longitude = task.Pin.Position.Longitude
                },
                Radius = (float)task.Radius/1000,
                StartTime = task.DateFrom.Date.Add(task.MeetingTime),
                Title = task.Title,
                Type = (int)task.Type + 1,
                TravelType = task.TravelMode.ToString().ToLower(),
            };

            if (_update && userTasks.Contains(task))
            {
                userTasks.Remove(task);
                await API.API.UpdateAsync(task.Id,model);
            }
              
                
            userTasks.Add(task);

            if (!_update)
            {
                var id = await API.API.PostAsync(model);
                task.Id = id;
            }

            await Navigation.PopAsync();
        }

        private bool IsValidForm()
        {
            if(String.IsNullOrWhiteSpace(task.Title))
            {
                DependencyService.Get<IMessage>().ShortAlert("Invalid title");
                return false;
            }

            if (task.Pin == null)
            {
                DependencyService.Get<IMessage>().ShortAlert("No location selected");
                return false;
            }

            if(task.Type == UserTaskType.Radius && task.Radius < 0)
            {
                DependencyService.Get<IMessage>().ShortAlert("Invalid radius");
                return false;
            }
            return true;
        }

        private void SetLocalization(object sender, EventArgs e)
        {
            var page = new NewTaskMapPage(task);
            Navigation.PushModalAsync(page);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            location.Text = task.Location;
        }

        private void TypePicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            if (picker.SelectedItem.ToString() == "Point")
            {
                task.Type = UserTaskType.Point;
                PointSettings.IsVisible = true;
                RadiusSettings.IsVisible = false;
            }
            else
            {
                task.Type = UserTaskType.Radius;
                PointSettings.IsVisible = false;
                RadiusSettings.IsVisible = true;
            }
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            var answer = await DisplayAlert("Remove task", "Are you sure you want to remove a task?", "Yes", "No");
            if(answer)
            {
                userTasks.Remove(task);

                await API.API.DeleteAsync(task.Id);

                await Navigation.PopAsync();
            }
        }

        private void TravelPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            task.TravelMode = (TravelType)picker.SelectedIndex;
        }
    }
}