﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AndroidApp.Models;
using Plugin.Geolocator;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms.Xaml;

namespace AndroidApp.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MapPage : ContentPage
	{
        private ObservableCollection<UserTask> userTasks;

        public MapPage ()
		{
			InitializeComponent ();
            userTasks = App.userTasks;
            myMap.UiSettings.MyLocationButtonEnabled = true;
            userTasks.CollectionChanged += UserTasks_CollectionChanged;
            CenterMyLocation();
            foreach (UserTask item in userTasks)
            {
                myMap.Pins.Add(item.Pin);
                myMap.UserTasks.Add(item);
            }
        }

        private async void CenterMyLocation()
        {
            var pos = await CrossGeolocator.Current.GetPositionAsync();
            myMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(pos.Latitude, pos.Longitude), Distance.FromKilometers(2)));
            myMap.MyLocationEnabled = true;
        }

        private void UserTasks_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
            {
                foreach (UserTask item in e.OldItems)
                {
                    myMap.Pins.Remove(item.Pin);
                    myMap.UserTasks.Remove(item);
                }
            }

            if (e.NewItems != null)
            {
                foreach (UserTask item in e.NewItems)
                {
                    myMap.Pins.Add(item.Pin);
                    myMap.UserTasks.Add(item);
                }
            }
        }
    }
}