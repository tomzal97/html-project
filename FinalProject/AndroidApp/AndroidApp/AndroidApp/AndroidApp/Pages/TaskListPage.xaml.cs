﻿using AndroidApp.Interfaces;
using AndroidApp.Models;
using AndroidApp.Models.Enums;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms.Xaml;

namespace AndroidApp.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TaskListPage : ContentPage
	{
        public ObservableCollection<UserTask> UserTasks { get; set; }

        private bool _isRefreshing = false;
        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set
            {
                _isRefreshing = value;
                OnPropertyChanged(nameof(IsRefreshing));
            }
        }

        public TaskListPage()
        {
            InitializeComponent();
            UserTasks = App.userTasks;
            BindingContext = this;

            Command refresh = new Command(async () =>
            {
                IsRefreshing = true;
                var record = await API.API.GetAsync();
                UserTasks.Clear();
                foreach (var v in record)
                {
                    var str = v.TravelType;
                    str = char.ToUpper(str[0]) + str.Substring(1);
                    UserTasks.Add(new UserTask
                    {
                        DateFrom = v.StartTime,
                        DateTo = v.EndTime ?? v.StartTime,
                        Description = v.Description,
                        Pin = new Pin
                        {
                            Type = PinType.Place,
                            Position = new Xamarin.Forms.GoogleMaps.Position(v.LocationLatLng.Latitude, v.LocationLatLng.Longitude),
                            Label = v.Title,
                            Address = v.Description
                        },
                        Radius = (double)(v.Radius ?? 0),
                        Title = v.Title,
                        Type = v.Type == 1 ? UserTaskType.Point : UserTaskType.Radius,
                        Id = v.Id,
                        MeetingTime = v.StartTime.TimeOfDay,
                        TravelMode = (TravelType)Enum.Parse(typeof(TravelType), str)
                    });
                }
                IsRefreshing = false;
            });

            taskList.RefreshCommand = refresh;
            taskList.SetBinding(ListView.IsRefreshingProperty, new Binding(nameof(IsRefreshing)));
        }

        private void ShowNewTaskPage(object sender, EventArgs e)
        {
            var page = new TaskDetailsPage();
            Navigation.PushAsync(page);
        }

        private void TaskList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var item = ((ListView)sender).SelectedItem as UserTask;
            var page = new TaskDetailsPage(update: true, updateTask: item);
            Navigation.PushAsync(page);
            //DependencyService.Get<IOnPlaceNotifcation>().ShowNotification(item.Id, "abc");
        }
    }
}