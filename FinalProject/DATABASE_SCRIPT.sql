-- Table: public.notifications

-- DROP TABLE public.notifications;

CREATE TABLE public.notifications
(
    "Id" integer NOT NULL DEFAULT nextval('notifications_id_seq'::regclass),
    "Description" character varying(256) COLLATE pg_catalog."default" NOT NULL,
    "Location" geography(Point,4326) NOT NULL,
    "StartTime" timestamp without time zone NOT NULL,
    "EndTime" timestamp without time zone NOT NULL,
    "Type" character varying(50) COLLATE pg_catalog."default",
    "Radius" real,
    CONSTRAINT notifications_pkey PRIMARY KEY ("Id")
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

-- ALTER TABLE public.notifications
--    OWNER to postgres;