﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Npgsql.EntityFrameworkCore.PostgreSQL.Infrastructure.Internal;
using Npgsql.EntityFrameworkCore.PostgreSQL.Internal;
using GISAPI.EntityFramework;
using Newtonsoft.Json;

namespace GISAPI
{
    public class Startup
    {
        public static IConfiguration config;
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            config = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddJsonOptions(options => {
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                }); ;
            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));
            services.AddEntityFrameworkNpgsql()
                  .AddDbContext<postgreGISContext>()
                  .BuildServiceProvider();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
    public class EFDesignTimeService : IDesignTimeServices
    {
        public void ConfigureDesignTimeServices(IServiceCollection services)
        {
            new EntityFrameworkRelationalServicesBuilder(services).TryAddProviderSpecificServices(x =>
            {
                x.TryAddSingleton<INpgsqlOptions, NpgsqlOptions>(p =>
                {
                    var dbOption = new DbContextOptionsBuilder()
                        .UseNpgsql(Startup.config.GetConnectionString("MyDatabase"),
                            ob => ob.UseNodaTime().UseNetTopologySuite()).Options;
                    var npgOptions = new NpgsqlOptions();
                    npgOptions.Initialize(dbOption);
                    return npgOptions;
                });
            });
        }
    }
}
