﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using GISAPI.Model;
using Microsoft.Extensions.Configuration;

namespace GISAPI.EntityFramework
{
    public partial class postgreGISContext : DbContext
    {
        public postgreGISContext()
        {
        }

        public postgreGISContext(DbContextOptions<postgreGISContext> options)
            : base(options)
        {
        }

        
        public virtual DbSet<Notification> Notifications { get; set; }

        public virtual DbSet<Model.Type> Types { get; set; }
        

        // Unable to generate entity type for table 'tiger.zip_lookup_all'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseNpgsql(Startup.config.GetConnectionString("MyDatabase"), o=>o.UseNetTopologySuite());
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasPostgresExtension("address_standardizer")
                .HasPostgresExtension("fuzzystrmatch")
                .HasPostgresExtension("ogr_fdw")
                .HasPostgresExtension("pgrouting")
                .HasPostgresExtension("pointcloud")
                .HasPostgresExtension("pointcloud_postgis")
                .HasPostgresExtension("postgis")
                .HasPostgresExtension("postgis_sfcgal")
                .HasPostgresExtension("postgis_tiger_geocoder")
                .HasPostgresExtension("postgis_topology");


            modelBuilder.Entity<Notification>(entity =>
            {
                entity.ToTable("notifications", "html");

                entity.Property(e => e.Id).UseNpgsqlIdentityAlwaysColumn();

                entity.Property(e => e.Description).HasMaxLength(256);

                entity.Property(e => e.EndTime).HasColumnType("timestamp(6) without time zone");

                entity.Property(e => e.LastPush).HasColumnType("timestamp(6) without time zone");

                entity.Property(e => e.StartTime).HasColumnType("timestamp(6) without time zone");

                entity.Property(e => e.Location).HasColumnType("geography");

                entity.Property(e => e.Title).HasMaxLength(256);
                entity.Property(e => e.TravelType).HasMaxLength(50);
                entity.Property(e => e.LastMessage).HasMaxLength(256);

            });

            modelBuilder.Entity<Model.Type>(entity =>
            {
                entity.ToTable("types", "html");

                entity.Property(e => e.Id).UseNpgsqlIdentityAlwaysColumn();

                entity.Property(e => e.Name).HasMaxLength(50);
            });


            //modelBuilder.HasSequence<int>("notifications_id_seq");
        }
    }
}
