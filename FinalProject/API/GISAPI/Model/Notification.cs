﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NetTopologySuite.Geometries;
using NodaTime;
using GISAPI.Model;

namespace GISAPI.Model
{

    public partial class Notification
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public int Type { get; set; }
        public float? Radius { get; set; }
        public string Title { get; set; }
        public string LastMessage { get; set; }
        public bool PushActive { get; set; }

        [Column(TypeName = "geography")]
        public Point Location { get; set; }
        public DateTime? LastPush { get; set; }
        public int? TravelTimeMins { get; set; }
        public string TravelType { get; set; }

        [NotMapped]
        public LocationLatLng LocationLatLng { get; set; }

        [NotMapped]
        public Type TypeN { get; set; }
    }
}
