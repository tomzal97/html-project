﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using NetTopologySuite.Geometries;

namespace GISAPI.Model
{
    public partial class Locations
    {
        public int NotificationId { get; set; }

        [Column(TypeName = "geography(Point,4326)")]
        public Point Location { get; set; }
    }
}
