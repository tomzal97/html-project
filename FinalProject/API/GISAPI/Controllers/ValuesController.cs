﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using GISAPI.EntityFramework;
using GISAPI.Model;
using Microsoft.AspNetCore.Cors;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace GISAPI.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("MyPolicy")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly postgreGISContext ctx;
       
        public ValuesController() {
            ctx = new postgreGISContext();
            foreach (var v in ctx.Notifications)
            {
                v.LocationLatLng = new LocationLatLng { Latitude = v.Location.Coordinates[0].X, Longitude = v.Location.Coordinates[0].Y };
                v.TypeN = (from t in ctx.Types
                           where t.Id == v.Type
                           select t).FirstOrDefault();
            }
        }

        // GET api/values
        [HttpGet]
        public List<Notification> Get()
        {
            return ctx.Notifications.ToList();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public Notification Get(int id)
        {
            return ctx.Notifications.FirstOrDefault(p => p.Id == id);
        }

        [HttpGet("travel")]
        public async Task<List<PushMessage>> GetAsync(double latitude, double longitude)
        {
            var httpClient = new HttpClient();
            
            var record = ctx.Notifications.Where(p => p.Type == 1 && p.PushActive);
            foreach(var v in record)
            {
                string uri = "https://maps.googleapis.com/maps/api/directions/json?" +
                       "mode="+v.TravelType+"&origin=" + latitude.ToString().Replace(',', '.') + "," + longitude.ToString().Replace(',', '.') + "&destination=" + v.LocationLatLng.Latitude.ToString().Replace(',', '.') + "," + v.LocationLatLng.Longitude.ToString().Replace(',', '.') +
                       "&key=AIzaSyAsFwIXA3W189c_WBwSJgiEFS92b4hEifY";
                var response = await httpClient.GetAsync(uri);

                string content = await response.Content.ReadAsStringAsync();

                var obj = JsonConvert.DeserializeObject<dynamic>(content);
                DateTime time;

                    int duration = Int32.Parse(obj.routes[0].legs[0].duration.value.ToString()) / 60;
                    if(duration>0)
                    {
                        int hours = duration / 60;
                        int minutes = duration - hours * 60;
                        time = v.StartTime.Subtract(new TimeSpan(hours, minutes + 15, 0));
                        if (time <= DateTime.Now)
                        {
                            var x = (time.AddMinutes(15)).Subtract(DateTime.Now).Minutes;
                            v.LastMessage = $"{v.Title}: Wyjdź w ciągu {x} min żeby zdążyć. Oczekiwany czas podróży: " + (hours > 0 ? hours + " h " : "") + minutes + " min";
                        v.LastPush = DateTime.Now;
                        v.PushActive = false;
                        }
                    }
           
                  
                

            };
            ctx.SaveChanges();
            return record.Where(r => !string.IsNullOrEmpty(r.LastMessage)).Select(r => new PushMessage { TaskId=r.Id, Message = r.LastMessage}).ToList();

            //will throw an exception if not successful
        }


        [HttpGet("withindist")]
        public async Task<List<PushMessage>> Getr(double latitude,double longitude)
        {
            var point = new NetTopologySuite.Geometries.Point(latitude, longitude);

            var record = ctx.Notifications.Where(p => p.PushActive && p.Type == 2).Where(p => p.Location.IsWithinDistance(point, (double)p.Radius/100)).Where(x=>x.LastPush==null || DateTime.Now.Subtract(x.LastPush.Value).TotalMinutes>=10);

            foreach (var v in record)
            {
                v.LastPush = DateTime.Now; ;
                var d = Math.Round(v.Location.Distance(point)*100, 3);
               
                v.LastMessage = $"{v.Title}: Jesteś w odległości "+(d>=1 ? Math.Round(d,2)+" km" : d*1000 + "m")+ " od celu.";
            }
               

            ctx.SaveChanges();

            var xu = await GetAsync(latitude,longitude);

            return ((record.Select(r => new PushMessage { TaskId = r.Id, Message = r.LastMessage})).Union(xu)).ToList();


        }

        // POST api/values
        [HttpPost]
        public int Post(LiteNotification Notification)
        {
            int id = 0;
                if (Notification != null)
                {
                    Notification n = new Notification();
                n.Title = Notification.Title;
                    n.Description = Notification.Description;
                    n.EndTime = Notification.EndTime;
                    n.StartTime = Notification.StartTime;
                    n.Radius = Notification.Radius;
                    n.Type = Notification.Type;
                    n.Location = new NetTopologySuite.Geometries.Point(Notification.Location.Latitude, Notification.Location.Longitude);
                    n.LocationLatLng = Notification.Location;
              
                n.TravelTimeMins = Notification.TravelTimeMins;
                n.TypeN = ctx.Types.FirstOrDefault(t => t.Id == Notification.Type);
                    //if(Notification.WeekDays != null) n.WeekDays = (from u in ctx.WeekDays
                    //              where (Notification.WeekDays).Contains(u.DayNo)
                    //              select u).ToList();
                n.PushActive = true;
                if(string.IsNullOrEmpty(Notification.TravelType))
                {
                    n.TravelType = "walking";
                }
                else
                {
                    n.TravelType = Notification.TravelType;
                }
             


                ctx.Notifications.Add(n);
                ctx.SaveChanges();
                id = ctx.Notifications.Max(p=>p.Id);
                //foreach (var v in Notification.WeekDays)
                //{
                //    ctx.CyclicNotifics.Add(new CyclicNotify { NotificationId = id, DayNo = v });
                //}
                ctx.SaveChanges();
                }
            return id;
            
        }



        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, LiteNotification Notification)
        {
            var n = ctx.Notifications.FirstOrDefault(x => x.Id == id);
            n.Title = Notification.Title;
            n.Description = Notification.Description;
            n.EndTime = Notification.EndTime;
            n.StartTime = Notification.StartTime;
            n.Radius = Notification.Radius;
            n.Type = Notification.Type;
            n.Location = new NetTopologySuite.Geometries.Point(Notification.Location.Latitude, Notification.Location.Longitude);
            n.LocationLatLng = Notification.Location;
            n.TravelType = Notification.TravelType;
            n.TravelTimeMins = Notification.TravelTimeMins;
            n.TypeN = ctx.Types.FirstOrDefault(t => t.Id == Notification.Type);
            //if(Notification.WeekDays != null) n.WeekDays = (from u in ctx.WeekDays
            //              where (Notification.WeekDays).Contains(u.DayNo)
            //              select u).ToList();
            ctx.SaveChanges();



        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var item = ctx.Notifications.FirstOrDefault(p => p.Id == id);
            ctx.Notifications.Remove(item);
            ctx.SaveChanges();
        }

        [HttpDelete]
        [Route("DeleteOld")]
        public void DeleteOld()
        {
            var record = ctx.Notifications.Where(x => (x.Type==1 && x.StartTime <= DateTime.Now) || x.EndTime <= DateTime.Now);
            foreach (var v in record)
                ctx.Notifications.Remove(v);
            ctx.SaveChanges();
        }
    }
}