﻿-- Database: postgres

-- DROP DATABASE postgres;

CREATE DATABASE postgres
    WITH 
    OWNER = azure_superuser
    ENCODING = 'UTF8'
    LC_COLLATE = 'English_United States.1252'
    LC_CTYPE = 'English_United States.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

COMMENT ON DATABASE postgres
    IS 'default administrative connection database';
-- SCHEMA: html

-- DROP SCHEMA html ;

CREATE SCHEMA html
    AUTHORIZATION htmladmin;
	
-- SCHEMA: shared_extensions

-- DROP SCHEMA shared_extensions ;

CREATE SCHEMA shared_extensions
    AUTHORIZATION htmladmin;

GRANT USAGE ON SCHEMA shared_extensions TO PUBLIC;

GRANT ALL ON SCHEMA shared_extensions TO htmladmin;
-- CREATE EXTENSION "shared_extensions".postgis

-- Table: html.notifications

-- DROP TABLE html.notifications;

CREATE TABLE html.notifications
(
    "Id" integer NOT NULL,
    "Title" character varying(256) COLLATE pg_catalog."default" NOT NULL,
    "Description" character varying(256) COLLATE pg_catalog."default",
    "Location" shared_extensions.geography(Point,4326) NOT NULL,
    "StartTime" timestamp(6) without time zone NOT NULL,
    "EndTime" timestamp(6) without time zone,
    "Type" integer NOT NULL,
    "Radius" real,
    "LastPush" timestamp(6) without time zone,
    "TravelType" character varying(50) COLLATE pg_catalog."default",
    "TravelTimeMins" integer,
    "PushActive" boolean NOT NULL,
    "LastMessage" character varying(256) COLLATE pg_catalog."default",
    CONSTRAINT notifications_pkey PRIMARY KEY ("Id")
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE html.notifications
    OWNER to htmladmin;
	
-- Table: html.types

-- DROP TABLE html.types;

CREATE TABLE html.types
(
    "Id" integer NOT NULL,
    "Name" character varying(50) COLLATE pg_catalog."default",
    CONSTRAINT types_pkey PRIMARY KEY ("Id")
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE html.types
    OWNER to htmladmin;



INSERT INTO html.notifications ("Id","Title","Description","Location","StartTime","EndTime","Type","Radius","LastPush","TravelType","TravelTimeMins","PushActive","LastMessage") VALUES 
(75,'Some task','Some description',0101000020E610000047E915BF111E4A400000A00A8A013540,'2019-01-13 23:00:00.000','2019-01-14 23:00:00.000',2,1,'2019-01-14 18:36:39.127','walking',0,true,'Some task: Jesteś w odległości 217m od celu.')
,(55,'German Goethe Institut','Ask about german course and prices',0101000020E610000034B9E1D9CB1D4A40000080707D043540,'2019-01-13 01:00:00.000','2019-01-18 00:00:00.000',2,1,'2019-01-14 18:18:23.168','walking',0,true,'German Goethe Institut: Jesteś w odległości 104m od celu.')
;

INSERT INTO html."types" ("Id","Name") VALUES 
(1,'Specific Location')
,(2,'Radius')
;